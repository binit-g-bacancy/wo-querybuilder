const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const cfg = {
    entry: ['babel-polyfill','./app.jsx'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name][hash].js',
        publicPath: ''
    },
    mode: 'development',
    optimization: {
        splitChunks: {
            chunks: 'all',
        },
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 5000
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.css$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' },
                ],
            },
        ],
    },
    resolve: {
        extensions: ['.jsx', '.js'],
        alias: {
            'react-dom': '@hot-loader/react-dom'
        }
    },
    plugins: [
        new HtmlWebpackPlugin({
          template: "./index.html",
          filename: "./index.html"
        })
      ],
      watch: true
};

module.exports = cfg;