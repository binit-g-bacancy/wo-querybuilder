import React, { useState, useEffect, useRef } from "react";
import PropTypes from 'prop-types';
import produce from "immer";
import './queryEditor.css';

const sortOpt = [
  { id: 'asc', name: 'Asc' },
  { id: 'desc', name: 'Desc' },
];

const defaultRecord = {
  name: '',
  label: '',
  sort: '',
  visible: true,
  filter: []
};

function QueryEditor(props) {
  const isRequiredProps = checkProps(props);
  if (isRequiredProps) return isRequiredProps;

  const [data, updateData] = useState({
    tableMeta: Object.assign({}, props.tableMeta),
    queryData: Object.assign({}, props.queryData),
    tableData: [],
    filterRows: []
  });
  const [activeTab, changeTab] = useState('query-grid');
  const [tempIndex, setTempIndex] = useState(null);
  const [result, updateResult] = useState({ fetching: false, element: null });
  const gridRows = [
    {
      name: 'name', label: 'Field', idCol: 'name', nameCol: 'name',
      colProps: (row, col) => {
        let used = props.queryData.fields.map(q => q.name)
        col.options = data.tableMeta.fields.filter(f => !used.includes(f.name) || f.name === row.name);
        return col;
      },
    },
    { name: 'label', label: 'Label', type: 'string' },
    { name: 'sort', label: 'Sort', options: sortOpt },
    { name: 'visible', label: 'Show', type: 'boolean' },
  ];

  useEffect(() => {
    if (props.tableMeta.table !== data.tableMeta.table || props.queryData.table !== data.queryData.table ||
      (JSON.stringify(props.tableMeta.fields) !== JSON.stringify(data.tableMeta.fields)) ||
      (JSON.stringify(props.queryData.fields) !== JSON.stringify(data.queryData.fields))) {
      updateData(produce(data, draft => {
        draft.tableMeta = { ...props.tableMeta };
        draft.queryData = { ...props.queryData };
      }));
    }
    const tableLength = data.tableData.filter(d => d.name).length;
    const metaLength = props.tableMeta.fields.filter(d => d.name).length;

    if (tableLength === data.tableData.length && tableLength < metaLength) {
      updateData(produce(data, draft => {
        const { filter, ...rest } = defaultRecord;
        let obj = { ...rest };
        data.filterRows.map(r => { obj[r.name] = ''; });
        draft.tableData.push(obj);
      }))
    }
    const maxLength = Math.max(...props.queryData.fields.map(f => f.filter && f.filter.length || 0), 0);
    const filterDiff = (data.filterRows.length - maxLength) > 1;
    const fldDiff = Math.abs(props.queryData.fields.filter(f => f.name).length - data.tableData.filter(f => f.name).length) > 0;

    if (filterDiff || fldDiff || (!data.tableData.length && !data.filterRows.length)) {
      setTableData();
    }
  });

  function setTableData() {
    const metaLength = props.tableMeta.fields.filter(d => d.name).length;
    let rows = [{ name: 'criteria', label: 'Criteria', type: 'string' }];
    let columns = Object.assign([], props.queryData && props.queryData.fields);
    let maxLength = Math.max(...props.queryData.fields.map(f => f.filter && f.filter.length || 0));
    let filterObj = { criteria: '' };

    if (!columns.length || (columns.length < metaLength && columns[columns.length - 1].name)) { columns.push(defaultRecord); }

    for (let i = 0; i <= maxLength; i++) {
      if (i !== 0) {
        filterObj['or' + i] = '';
        rows.push({ name: 'or' + i, label: 'OR', type: 'string' })
      }
    }
    columns = columns.map(d => {
      let obj = { ...defaultRecord, ...filterObj, ...d };
      obj.filter.map((f, i) => { obj[i === 0 ? 'criteria' : ('or' + i)] = f || ''; });
      delete obj.filter;
      return obj;
    });
    updateData(produce(data, draft => {
      draft.filterRows = rows;
      draft.tableData = columns;
    }));
  }

  const toggleTab = async grid => {
    if (activeTab !== grid) {
      if (grid === 'result-grid' && !data.queryData.table) {
        toastr.warning('Table is required');
        return;
      }
      changeTab(produce(activeTab, draft => grid));
      if (grid === 'result-grid') {
        updateResult(produce(result, draft => { draft.fetching = true; }));
        const res = await props.onQuery({ ...data.queryData });
        if (res) {
          updateResult(produce(result,
            draft => {
              draft.fetching = false;
              draft.element = res;
            }));
        }
      }
    }
  }

  const onChange = (value, index, isfilterRow = false, filterIndex) => {
    let queryData = { ...data.queryData };
    let queryDataObj = { ...defaultRecord, ...queryData.fields[index] };
    let keys = Object.keys(value);

    if (keys[0] === 'name' && !value.name) {
      let array = [...queryData.fields];
      array = array.filter((d, i) => i !== index);
      queryData.fields = array;
    } else {
      if (isfilterRow) {
        let array = [...queryDataObj['filter']];
        array[filterIndex] = value[keys[0]];
        queryDataObj['filter'] = array;
      }
      else {
        queryDataObj = { ...queryDataObj, ...value };
      }
      let array = [...queryData.fields];
      array[index] = queryDataObj;
      queryData.fields = array;
    }
    if (isfilterRow && !value[keys[0]]) {
      queryData.fields = queryData.fields.map(f => {
        let obj = Object.assign({}, f);
        if (filterIndex < f.filter.length && !f.filter[filterIndex]) {
          obj.filter = obj.filter.filter((f, i) => i !== filterIndex);
        }
        return obj;
      })
    }
    props.onChange(queryData);

    updateData(produce(data, draft => {
      const filterLen = data.filterRows.length;
      if (keys[0] === data.filterRows[filterLen - 1].name) {
        draft.tableData.map(t => {
          t['or' + filterLen] = '';
        })
        draft.filterRows.push({ name: 'or' + filterLen, label: 'OR', type: 'string' })
      }
      if (keys[0] === 'name' && !value.name) {
        draft.tableData.filter((d, i) => i !== index);
      } else {
        draft.tableData[index] = { ...draft.tableData[index], ...value };
      }
    }));
  }

  function updateTempIndex(index) {
    if (index !== tempIndex) {
      setTempIndex(produce(tempIndex, draft => index))
    }
  }

  function onDrop(ev, i) {
    ev.preventDefault();
    let arr = [...data.queryData.fields];
    let tempData = arr.splice(tempIndex, 1)[0] || defaultRecord;
    arr.splice(i, 0, tempData);
    props.onChange({ ...data.queryData, fields: arr });
    updateData(produce(data, draft => {
      let tempData = draft.tableData.splice(tempIndex, 1)[0];
      draft.tableData.splice(i, 0, tempData)
    }));
  }

  function queryGrid() {
    const titleList = [...gridRows, ...data.filterRows];
    return <div className='data-grid scrollbar'>
      <div className='grid-row'>
        <div key='title'>
          {titleList.map((row, index) => (<div key={index} className='grid-col'>{row.label}</div>))}
        </div>
        {data.tableData.map((col, i) => (
          <div key={i}
            className='grid-list'
            draggable={col.name ? "true" : "false"}
            onDragStart={() => { if (col.name) updateTempIndex(i) }}
            onDragOver={(ev) => { ev.preventDefault(); }}
            onDrop={(ev) => { if (col.name) onDrop(ev, i) }}>
            {gridRows.map((row, index) => (
              <div key={index} className='grid-col'>
                {index === 0 && <div className="icon"><i className='drag-icon'></i></div>}
                <Field meta={row} form={col} index={i} onChange={onChange} />
              </div>))}
            {data.filterRows.map((row, index) => (<div key={index} className='grid-col'>
              <Field meta={row} form={col} index={i} onChange={(v, j) => onChange(v, j, true, index)} />
            </div>
            ))}
          </div>
        ))}
      </div>
    </div >
  }

  function queryPreview() {
    return <div className='p10'>
      <textarea className='query-preview scrollbar' key={JSON.stringify(data.queryData, ' ', 2)} defaultValue={JSON.stringify(data.queryData, ' ', 2)} />
    </div>
  }

  function renderResult() {
    if (result.fetching) return <div className='p10'>Loading...</div>;
    return <div className='p10 w100 scrollbar'>{result.element ? result.element : null}</div>
  }

  return (<div className='query-editor'>
    <h4>Query Editor</h4>
    <div className='tab'>
      <button className={`tab-item${activeTab === 'query-grid' ? ' tab-active-item' : ''} `} onClick={() => toggleTab('query-grid')}>Query</button>
      <button className={`tab-item${activeTab === 'result-grid' ? ' tab-active-item' : ''} `} onClick={() => toggleTab('result-grid')}>Result</button>
    </div>
    <div className='grid-body'>
      {activeTab === 'query-grid' ? (
        <div className='grid-content'>
          {queryGrid()}
          {queryPreview()}
        </div>
      ) : null}
      {activeTab === 'result-grid' ? (
        <div className='grid-content'>
          {renderResult()}
        </div>
      ) : null}
    </div>
  </div>);
}

function Field(props) {
  let inputProps = {
    name: props.meta.name,
    ref: useRef(null),
    value: props.form[props.meta.name],
    onChange: e => props.onChange({ [e.target.name]: e.target.value }, props.index),
    ...props.inputProps
  };
  const fieldProps = props.meta;
  if (fieldProps.colProps) {
    fieldProps.colProps(props.form, props.meta);
  };
  if (fieldProps.options) {
    let options = fieldProps.options.map((opt, i) => (
      <option key={i} value={opt[fieldProps.idCol ? fieldProps.idCol : 'id']}>
        {opt[fieldProps.nameCol ? fieldProps.nameCol : 'name']}
      </option>));
    options.unshift(<option key="empty" value={null}> </option>)
    return <select className='w100' {...inputProps}>
      {options}
    </select>
  }
  if (fieldProps.type === 'boolean') {
    inputProps.type = 'checkbox';
    inputProps.checked = inputProps.value;
    inputProps.onChange = e => props.onChange({ [e.target.name]: e.target.checked }, props.index);
  }
  if (fieldProps.type === 'string') inputProps.type = 'text';
  if (fieldProps.type === 'number') inputProps.type = 'number';
  if (fieldProps.type === 'date') inputProps.type = 'date';
  return <input {...inputProps} />
}

function checkProps(props) {
  let error = null;
  const requiredProps = ['tableMeta', 'queryData', 'onChange', 'onQuery'];
  const propType = {
    tableMeta: { type: 'object', keys: [{ name: 'table', type: 'string' }, { name: 'fields', type: 'array' }] },
    queryData: { type: 'object', keys: [{ name: 'table', type: 'string' }, { name: 'fields', type: 'array' }] },
    onChange: { type: 'function' },
    onQuery: { type: 'function' },
  }
  const isRrequired = requiredProps.filter(k => props[k] === undefined);

  if (isRrequired.length > 0) {
    error = isRrequired.map(r => <p key={r}>{`${r} prop is Required.`}</p>);
  } else {
    let errList = [];
    requiredProps.map(k => {
      if ((Array.isArray(props[k]) && propType[k]['type'] !== 'array') || (propType[k]['type'] === 'array' && !Array.isArray(props[k]) && typeof (props[k]) !== propType[k]['type'])) {
        errList.push(<div key={k}>{`${k} sholud be type of ${propType[k]['type']}.`}</div>);
      }
      else if (propType[k]['keys']) {
        propType[k]['keys'].map(key => {
          const type = Array.isArray(props[k][key.name]) ? 'array' : typeof (props[k][key.name]);

          if ((key.type !== 'array' && Array.isArray(props[k][key.name])) ||
            (key.type === 'array' && !Array.isArray(props[k][key.name]) &&
              typeof (props[k][key.name]) !== key.type)) {
            errList.push(<div>{`Invalid prop '${k}.${key.name}' of type '${type}' supplied to QueryEditor, expected ${key.type}.`}</div>);
          }
        })
      }
    })
    if (errList.length > 0) error = errList;
  }

  if (error) return (<div className='query-editor'>
    <h4>Query Editor</h4>
    {error}
  </div>)
  return null;
}

QueryEditor.propTypes = {
  queryData: PropTypes.shape({
    table: PropTypes.string,
    fields: PropTypes.arrayOf(PropTypes.object)
  }).isRequired,
  tableMeta: PropTypes.shape({
    table: PropTypes.string,
    fields: PropTypes.arrayOf(PropTypes.object)
  }).isRequired,
  onChange: PropTypes.func.isRequired,
  onQuery: PropTypes.func.isRequired,
};

export default QueryEditor;