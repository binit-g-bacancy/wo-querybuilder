import React from "react";
import QueryEditor from './queryEditor/queryEditor';
import dataList from './result.json';

const tableMeta = {
  table: "asset_hdr",
  fields: [
    { name: "acct_no", data_type: "nvarchar" },
    { name: "activity_code", data_type: "nvarchar" },
    { name: "actual_finish_dt", data_type: "datetime" },
    { name: "actual_start_dt", data_type: "datetime" },
    { name: "add_by", data_type: "nvarchar" },
    { name: "add_dt", data_type: "datetime" },
    { name: "appvby", data_type: "nvarchar" },
    { name: "assign_to", data_type: "nvarchar" },
    { name: "attach_link_id", data_type: "int" },
    { name: "billable", data_type: "nchar" },
    { name: "closed", data_type: "datetime" },
    { name: "cmpm_flag", data_type: "nvarchar" },
    { name: "crew_leader", data_type: "nvarchar" },
    { name: "customer_id", data_type: "nvarchar" },
    { name: "est_finish_dt", data_type: "datetime" },
    { name: "est_qty", data_type: "numeric" },
    { name: "est_start_dt", data_type: "datetime" },
    { name: "est_total", data_type: "numeric" },
    { name: "geometry", data_type: "geometry" },
    { name: "issue_id", data_type: "int" },
    { name: "issued", data_type: "datetime" },
    { name: "master_wo", data_type: "nvarchar" },
    { name: "mgmt_unit", data_type: "nvarchar" },
    { name: "op_id", data_type: "int" },
    { name: "origin_code", data_type: "nvarchar" },
    { name: "planby", data_type: "nvarchar" },
    { name: "portal_id", data_type: "int" },
    { name: "priority", data_type: "nvarchar" },
    { name: "project_no", data_type: "nvarchar" },
    { name: "reason_dntime", data_type: "nvarchar" },
    { name: "rec_type", data_type: "nchar" },
    { name: "req_date", data_type: "datetime" },
    { name: "req_department", data_type: "nvarchar" },
    { name: "sch_no", data_type: "int" },
    { name: "status", data_type: "nchar" },
    { name: "supervisor", data_type: "nvarchar" },
    { name: "target", data_type: "datetime" },
    { name: "tm_no", data_type: "nvarchar" },
    { name: "tran_no", data_type: "nvarchar" },
    { name: "upd_by", data_type: "nvarchar" },
    { name: "upd_dt", data_type: "datetime" },
    { name: "wk_in_progress", data_type: "nvarchar" },
    { name: "wo_comments", data_type: "nvarchar" },
    { name: "wo_compl", data_type: "int" },
    { name: "wo_descr", data_type: "nvarchar" },
    { name: "wo_no", data_type: "nvarchar" },
    { name: "wo_type", data_type: "nvarchar" },
    { name: "wr_no", data_type: "nvarchar" }
  ]
}
const cellStyle = {
  border: '1px solid rgb(169, 162, 162)',
  textAlign: 'left',
  padding: '8px'
}
const headerStyle = {
  borderCollapse: 'collapse',
  width: '100%'
}

function getResult(data) {
  if (!data.fields.length > 0) {
    return;
  }
  let sorted = {};
  data.fields.filter(d => d.sort && d.name).map(d => sorted[d.name] = d.sort);
  let result = dataList;
  const cols = data.fields.filter(d => d.visible && d.name).map(d => d.name);
  data.fields.map(d => {
    let orRes = [];
    if (d.name && d.filter && d.filter.length > 0) {
      d.filter.map(f => {
        let andRes = [];
        if (f && typeof (f) === 'string') {
          const arr = f.split(',');
          arr.map(str => {
            let operator = null;
            let value = null;
            let condition = (r) => null;
            const format = /[ %=<>]+|\b(\w*like|not like\w*)\b/; if (str.match(format)) {
              operator = str.match(format)[0].trim();
              value = str.replace(operator, '').trim();
              if (operator === 'like' || operator === '%') {
                value = str.replace('%', '');
                if (str.startsWith('%') && !str.endsWith('%')) condition = (r) => (r[d.name] && value && r[d.name].endsWith(value) || false);
                if (str.endsWith('%') && !str.startsWith('%')) condition = (r) => (r[d.name] && value && r[d.name].startsWith(value) || false);
                if (str.startsWith('%') && str.endsWith('%')) condition = (r) => (r[d.name] && value && r[d.name].includes(value) || false);
              } else if (operator === 'not like') {
                condition = (r) => (r[d.name] && value && !r[d.name].includes(value) || false);
              } else if (['<>', '=', '>=', '>', '<', '<='].includes(operator)) {
                if (operator === '<>') condition = (r) => (r[d.name] && value && r[d.name].toLowerCase() != (value).toLowerCase() || false);
                if (operator === '=') condition = (r) => (r[d.name] && value && r[d.name].toLowerCase() == (value).toLowerCase() || false);
                if (operator === '>') condition = (r) => (r[d.name] && value && r[d.name].toLowerCase() > (value).toLowerCase() || false);
                if (operator === '<') condition = (r) => (r[d.name] && value && r[d.name].toLowerCase() < (value).toLowerCase() || false);
                if (operator === '<=') condition = (r) => (r[d.name] && value && r[d.name].toLowerCase() <= (value).toLowerCase() || false);
                if (operator === '>=') condition = (r) => (r[d.name] && value && r[d.name].toLowerCase() >= (value).toLowerCase() || false);
              }
            } else if (str.match(/^[_A-z0-9]*((-|\s)*[_A-z0-9])*$/)) {
              condition = (r) => (r[d.name] && str && r[d.name] == str || false);
            }
            andRes = result.filter(r => condition(r));
          });
          orRes.push(...andRes);
        }
      });
      result = [...orRes];
    }
  });
  Object.keys(sorted).map(key => {
    result = result.sort(function (a, b) {
      let keyA = a[key];
      let keyB = b[key];
      if (keyA < keyB) return sorted[key] === 'asc' ? -1 : 1;
      if (keyA > keyB) return sorted[key] === 'asc' ? 1 : -1;
      return 0;
    });
  });

  result = result.map(r => {
    let obj = {};
    cols.map(c => { obj[c] = r[c] });
    return obj;
  });
  return result;
}

export default class QueryBuilderExample extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      queryData: {
        table: "asset_hdr",
        fields: [
          { name: 'wo_no', label: 'Work order', filter: ["FAC%","not like PK"], sort: 'asc', visible: true },
          { name: 'wo_type', label: 'Type', filter: ["W%", "CM"], sort: '', visible: true },
          { name: 'status', label: 'Status', filter: [], sort: '', visible: false }
        ],
      },
    }
  }

  renderResult = (data) => {
    const colTitles = this.state.queryData.fields.filter(q => q.visible && q.name).map(q => ({ name: q.name, label: q.label || q.name }));

    return <div>
      <p>Total Count: <b>{data.length}</b></p>
      <table style={headerStyle}>
        <thead>
          <tr>
            {colTitles.map(t => <th key={t.name} style={cellStyle}>{t.label}</th>)}
          </tr>
        </thead>
        <tbody>
          {data.map((d, i) => (<tr key={i}>
            {colTitles.map(t => <td key={t.name} style={cellStyle}>{d[t.name]}</td>)}
          </tr>))}
        </tbody>
      </table>
    </div>
  }

  onQuery = async (queryData) => {
    let response = getResult(queryData);
    if (!response) {
      this.setState({ result: [] })
      return <div>Select query to see the Result.</div>;
    }
    const html = this.renderResult(response);
    return html;
  }

  setQueryData = (queryData) => {
    this.setState({ queryData: { ...this.state.queryData, ...queryData } });
  }

  render() {
    return (
      <div>
        <QueryEditor
          queryData={this.state.queryData}
          tableMeta={tableMeta}
          onChange={this.setQueryData}
          onQuery={this.onQuery}
        />
      </div>
    );
  }
}