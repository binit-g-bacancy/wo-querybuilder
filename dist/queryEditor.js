"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _immer = _interopRequireDefault(require("immer"));

require("./queryEditor.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var sortOpt = [{
  id: 'asc',
  name: 'Asc'
}, {
  id: 'desc',
  name: 'Desc'
}];
var defaultRecord = {
  name: '',
  label: '',
  sort: '',
  visible: true,
  filter: []
};

function QueryEditor(props) {
  var isRequiredProps = checkProps(props);
  if (isRequiredProps) return isRequiredProps;

  var _useState = (0, _react.useState)({
    tableMeta: Object.assign({}, props.tableMeta),
    queryData: Object.assign({}, props.queryData),
    tableData: [],
    filterRows: []
  }),
      _useState2 = _slicedToArray(_useState, 2),
      data = _useState2[0],
      updateData = _useState2[1];

  var _useState3 = (0, _react.useState)('query-grid'),
      _useState4 = _slicedToArray(_useState3, 2),
      activeTab = _useState4[0],
      changeTab = _useState4[1];

  var _useState5 = (0, _react.useState)(null),
      _useState6 = _slicedToArray(_useState5, 2),
      tempIndex = _useState6[0],
      setTempIndex = _useState6[1];

  var _useState7 = (0, _react.useState)({
    fetching: false,
    element: null
  }),
      _useState8 = _slicedToArray(_useState7, 2),
      result = _useState8[0],
      updateResult = _useState8[1];

  var gridRows = [{
    name: 'name',
    label: 'Field',
    idCol: 'name',
    nameCol: 'name',
    colProps: function colProps(row, col) {
      var used = props.queryData.fields.map(function (q) {
        return q.name;
      });
      col.options = data.tableMeta.fields.filter(function (f) {
        return !used.includes(f.name) || f.name === row.name;
      });
      return col;
    }
  }, {
    name: 'label',
    label: 'Label',
    type: 'string'
  }, {
    name: 'sort',
    label: 'Sort',
    options: sortOpt
  }, {
    name: 'visible',
    label: 'Show',
    type: 'boolean'
  }];
  (0, _react.useEffect)(function () {
    if (props.tableMeta.table !== data.tableMeta.table || props.queryData.table !== data.queryData.table || JSON.stringify(props.tableMeta.fields) !== JSON.stringify(data.tableMeta.fields) || JSON.stringify(props.queryData.fields) !== JSON.stringify(data.queryData.fields)) {
      updateData((0, _immer["default"])(data, function (draft) {
        draft.tableMeta = _objectSpread({}, props.tableMeta);
        draft.queryData = _objectSpread({}, props.queryData);
      }));
    }

    var tableLength = data.tableData.filter(function (d) {
      return d.name;
    }).length;
    var metaLength = props.tableMeta.fields.filter(function (d) {
      return d.name;
    }).length;

    if (tableLength === data.tableData.length && tableLength < metaLength) {
      updateData((0, _immer["default"])(data, function (draft) {
        var filter = defaultRecord.filter,
            rest = _objectWithoutProperties(defaultRecord, ["filter"]);

        var obj = _objectSpread({}, rest);

        data.filterRows.map(function (r) {
          obj[r.name] = '';
        });
        draft.tableData.push(obj);
      }));
    }

    var maxLength = Math.max.apply(Math, _toConsumableArray(props.queryData.fields.map(function (f) {
      return f.filter && f.filter.length || 0;
    })).concat([0]));
    var filterDiff = data.filterRows.length - maxLength > 1;
    var fldDiff = Math.abs(props.queryData.fields.filter(function (f) {
      return f.name;
    }).length - data.tableData.filter(function (f) {
      return f.name;
    }).length) > 0;

    if (filterDiff || fldDiff || !data.tableData.length && !data.filterRows.length) {
      setTableData();
    }
  });

  function setTableData() {
    var metaLength = props.tableMeta.fields.filter(function (d) {
      return d.name;
    }).length;
    var rows = [{
      name: 'criteria',
      label: 'Criteria',
      type: 'string'
    }];
    var columns = Object.assign([], props.queryData && props.queryData.fields);
    var maxLength = Math.max.apply(Math, _toConsumableArray(props.queryData.fields.map(function (f) {
      return f.filter && f.filter.length || 0;
    })));
    var filterObj = {
      criteria: ''
    };

    if (!columns.length || columns.length < metaLength && columns[columns.length - 1].name) {
      columns.push(defaultRecord);
    }

    for (var i = 0; i <= maxLength; i++) {
      if (i !== 0) {
        filterObj['or' + i] = '';
        rows.push({
          name: 'or' + i,
          label: 'OR',
          type: 'string'
        });
      }
    }

    columns = columns.map(function (d) {
      var obj = _objectSpread({}, defaultRecord, {}, filterObj, {}, d);

      obj.filter.map(function (f, i) {
        obj[i === 0 ? 'criteria' : 'or' + i] = f || '';
      });
      delete obj.filter;
      return obj;
    });
    updateData((0, _immer["default"])(data, function (draft) {
      draft.filterRows = rows;
      draft.tableData = columns;
    }));
  }

  var toggleTab = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(grid) {
      var res;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (!(activeTab !== grid)) {
                _context.next = 11;
                break;
              }

              if (!(grid === 'result-grid' && !data.queryData.table)) {
                _context.next = 4;
                break;
              }

              toastr.warning('Table is required');
              return _context.abrupt("return");

            case 4:
              changeTab((0, _immer["default"])(activeTab, function (draft) {
                return grid;
              }));

              if (!(grid === 'result-grid')) {
                _context.next = 11;
                break;
              }

              updateResult((0, _immer["default"])(result, function (draft) {
                draft.fetching = true;
              }));
              _context.next = 9;
              return props.onQuery(_objectSpread({}, data.queryData));

            case 9:
              res = _context.sent;

              if (res) {
                updateResult((0, _immer["default"])(result, function (draft) {
                  draft.fetching = false;
                  draft.element = res;
                }));
              }

            case 11:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function toggleTab(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  var _onChange = function onChange(value, index) {
    var isfilterRow = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var filterIndex = arguments.length > 3 ? arguments[3] : undefined;

    var queryData = _objectSpread({}, data.queryData);

    var queryDataObj = _objectSpread({}, defaultRecord, {}, queryData.fields[index]);

    var keys = Object.keys(value);

    if (keys[0] === 'name' && !value.name) {
      var array = _toConsumableArray(queryData.fields);

      array = array.filter(function (d, i) {
        return i !== index;
      });
      queryData.fields = array;
    } else {
      if (isfilterRow) {
        var _array2 = _toConsumableArray(queryDataObj['filter']);

        _array2[filterIndex] = value[keys[0]];
        queryDataObj['filter'] = _array2;
      } else {
        queryDataObj = _objectSpread({}, queryDataObj, {}, value);
      }

      var _array = _toConsumableArray(queryData.fields);

      _array[index] = queryDataObj;
      queryData.fields = _array;
    }

    if (isfilterRow && !value[keys[0]]) {
      queryData.fields = queryData.fields.map(function (f) {
        var obj = Object.assign({}, f);

        if (filterIndex < f.filter.length && !f.filter[filterIndex]) {
          obj.filter = obj.filter.filter(function (f, i) {
            return i !== filterIndex;
          });
        }

        return obj;
      });
    }

    props.onChange(queryData);
    updateData((0, _immer["default"])(data, function (draft) {
      var filterLen = data.filterRows.length;

      if (keys[0] === data.filterRows[filterLen - 1].name) {
        draft.tableData.map(function (t) {
          t['or' + filterLen] = '';
        });
        draft.filterRows.push({
          name: 'or' + filterLen,
          label: 'OR',
          type: 'string'
        });
      }

      if (keys[0] === 'name' && !value.name) {
        draft.tableData.filter(function (d, i) {
          return i !== index;
        });
      } else {
        draft.tableData[index] = _objectSpread({}, draft.tableData[index], {}, value);
      }
    }));
  };

  function updateTempIndex(index) {
    if (index !== tempIndex) {
      setTempIndex((0, _immer["default"])(tempIndex, function (draft) {
        return index;
      }));
    }
  }

  function _onDrop(ev, i) {
    ev.preventDefault();

    var arr = _toConsumableArray(data.queryData.fields);

    var tempData = arr.splice(tempIndex, 1)[0] || defaultRecord;
    arr.splice(i, 0, tempData);
    props.onChange(_objectSpread({}, data.queryData, {
      fields: arr
    }));
    updateData((0, _immer["default"])(data, function (draft) {
      var tempData = draft.tableData.splice(tempIndex, 1)[0];
      draft.tableData.splice(i, 0, tempData);
    }));
  }

  function queryGrid() {
    var titleList = [].concat(gridRows, _toConsumableArray(data.filterRows));
    return /*#__PURE__*/_react["default"].createElement("div", {
      className: "data-grid scrollbar"
    }, /*#__PURE__*/_react["default"].createElement("div", {
      className: "grid-row"
    }, /*#__PURE__*/_react["default"].createElement("div", {
      key: "title"
    }, titleList.map(function (row, index) {
      return /*#__PURE__*/_react["default"].createElement("div", {
        key: index,
        className: "grid-col"
      }, row.label);
    })), data.tableData.map(function (col, i) {
      return /*#__PURE__*/_react["default"].createElement("div", {
        key: i,
        className: "grid-list",
        draggable: col.name ? "true" : "false",
        onDragStart: function onDragStart() {
          if (col.name) updateTempIndex(i);
        },
        onDragOver: function onDragOver(ev) {
          ev.preventDefault();
        },
        onDrop: function onDrop(ev) {
          if (col.name) _onDrop(ev, i);
        }
      }, gridRows.map(function (row, index) {
        return /*#__PURE__*/_react["default"].createElement("div", {
          key: index,
          className: "grid-col"
        }, index === 0 && /*#__PURE__*/_react["default"].createElement("div", {
          className: "icon"
        }, /*#__PURE__*/_react["default"].createElement("i", {
          className: "drag-icon"
        })), /*#__PURE__*/_react["default"].createElement(Field, {
          meta: row,
          form: col,
          index: i,
          onChange: _onChange
        }));
      }), data.filterRows.map(function (row, index) {
        return /*#__PURE__*/_react["default"].createElement("div", {
          key: index,
          className: "grid-col"
        }, /*#__PURE__*/_react["default"].createElement(Field, {
          meta: row,
          form: col,
          index: i,
          onChange: function onChange(v, j) {
            return _onChange(v, j, true, index);
          }
        }));
      }));
    })));
  }

  function queryPreview() {
    return /*#__PURE__*/_react["default"].createElement("div", {
      className: "p10"
    }, /*#__PURE__*/_react["default"].createElement("textarea", {
      className: "query-preview scrollbar",
      key: JSON.stringify(data.queryData, ' ', 2),
      defaultValue: JSON.stringify(data.queryData, ' ', 2)
    }));
  }

  function renderResult() {
    if (result.fetching) return /*#__PURE__*/_react["default"].createElement("div", {
      className: "p10"
    }, "Loading...");
    return /*#__PURE__*/_react["default"].createElement("div", {
      className: "p10 w100 scrollbar"
    }, result.element ? result.element : null);
  }

  return /*#__PURE__*/_react["default"].createElement("div", {
    className: "query-editor"
  }, /*#__PURE__*/_react["default"].createElement("h4", null, "Query Editor"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "tab"
  }, /*#__PURE__*/_react["default"].createElement("button", {
    className: "tab-item".concat(activeTab === 'query-grid' ? ' tab-active-item' : '', " "),
    onClick: function onClick() {
      return toggleTab('query-grid');
    }
  }, "Query"), /*#__PURE__*/_react["default"].createElement("button", {
    className: "tab-item".concat(activeTab === 'result-grid' ? ' tab-active-item' : '', " "),
    onClick: function onClick() {
      return toggleTab('result-grid');
    }
  }, "Result")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "grid-body"
  }, activeTab === 'query-grid' ? /*#__PURE__*/_react["default"].createElement("div", {
    className: "grid-content"
  }, queryGrid(), queryPreview()) : null, activeTab === 'result-grid' ? /*#__PURE__*/_react["default"].createElement("div", {
    className: "grid-content"
  }, renderResult()) : null));
}

function Field(props) {
  var inputProps = _objectSpread({
    name: props.meta.name,
    ref: (0, _react.useRef)(null),
    value: props.form[props.meta.name],
    onChange: function onChange(e) {
      return props.onChange(_defineProperty({}, e.target.name, e.target.value), props.index);
    }
  }, props.inputProps);

  var fieldProps = props.meta;

  if (fieldProps.colProps) {
    fieldProps.colProps(props.form, props.meta);
  }

  ;

  if (fieldProps.options) {
    var options = fieldProps.options.map(function (opt, i) {
      return /*#__PURE__*/_react["default"].createElement("option", {
        key: i,
        value: opt[fieldProps.idCol ? fieldProps.idCol : 'id']
      }, opt[fieldProps.nameCol ? fieldProps.nameCol : 'name']);
    });
    options.unshift( /*#__PURE__*/_react["default"].createElement("option", {
      key: "empty",
      value: null
    }, " "));
    return /*#__PURE__*/_react["default"].createElement("select", _extends({
      className: "w100"
    }, inputProps), options);
  }

  if (fieldProps.type === 'boolean') {
    inputProps.type = 'checkbox';
    inputProps.checked = inputProps.value;

    inputProps.onChange = function (e) {
      return props.onChange(_defineProperty({}, e.target.name, e.target.checked), props.index);
    };
  }

  if (fieldProps.type === 'string') inputProps.type = 'text';
  if (fieldProps.type === 'number') inputProps.type = 'number';
  if (fieldProps.type === 'date') inputProps.type = 'date';
  return /*#__PURE__*/_react["default"].createElement("input", inputProps);
}

function checkProps(props) {
  var error = null;
  var requiredProps = ['tableMeta', 'queryData', 'onChange', 'onQuery'];
  var propType = {
    tableMeta: {
      type: 'object',
      keys: [{
        name: 'table',
        type: 'string'
      }, {
        name: 'fields',
        type: 'array'
      }]
    },
    queryData: {
      type: 'object',
      keys: [{
        name: 'table',
        type: 'string'
      }, {
        name: 'fields',
        type: 'array'
      }]
    },
    onChange: {
      type: 'function'
    },
    onQuery: {
      type: 'function'
    }
  };
  var isRrequired = requiredProps.filter(function (k) {
    return props[k] === undefined;
  });

  if (isRrequired.length > 0) {
    error = isRrequired.map(function (r) {
      return /*#__PURE__*/_react["default"].createElement("p", {
        key: r
      }, "".concat(r, " prop is Required."));
    });
  } else {
    var errList = [];
    requiredProps.map(function (k) {
      if (Array.isArray(props[k]) && propType[k]['type'] !== 'array' || propType[k]['type'] === 'array' && !Array.isArray(props[k]) && _typeof(props[k]) !== propType[k]['type']) {
        errList.push( /*#__PURE__*/_react["default"].createElement("div", {
          key: k
        }, "".concat(k, " sholud be type of ").concat(propType[k]['type'], ".")));
      } else if (propType[k]['keys']) {
        propType[k]['keys'].map(function (key) {
          var type = Array.isArray(props[k][key.name]) ? 'array' : _typeof(props[k][key.name]);

          if (key.type !== 'array' && Array.isArray(props[k][key.name]) || key.type === 'array' && !Array.isArray(props[k][key.name]) && _typeof(props[k][key.name]) !== key.type) {
            errList.push( /*#__PURE__*/_react["default"].createElement("div", null, "Invalid prop '".concat(k, ".").concat(key.name, "' of type '").concat(type, "' supplied to QueryEditor, expected ").concat(key.type, ".")));
          }
        });
      }
    });
    if (errList.length > 0) error = errList;
  }

  if (error) return /*#__PURE__*/_react["default"].createElement("div", {
    className: "query-editor"
  }, /*#__PURE__*/_react["default"].createElement("h4", null, "Query Editor"), error);
  return null;
}

QueryEditor.propTypes = {
  queryData: _propTypes["default"].shape({
    table: _propTypes["default"].string,
    fields: _propTypes["default"].arrayOf(_propTypes["default"].object)
  }).isRequired,
  tableMeta: _propTypes["default"].shape({
    table: _propTypes["default"].string,
    fields: _propTypes["default"].arrayOf(_propTypes["default"].object)
  }).isRequired,
  onChange: _propTypes["default"].func.isRequired,
  onQuery: _propTypes["default"].func.isRequired
};
var _default = QueryEditor;
exports["default"] = _default;