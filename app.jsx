import React from 'react';
import ReactDOM from 'react-dom';
import QueryBuilderExample from "./src/index";

ReactDOM.render(
    <QueryBuilderExample />,
    window.document.getElementById('root'),
);